package lccv.springbootatividade.domain.models;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

import org.hibernate.annotations.Type;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Professor {
    
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Type(type="uuid-char")
    private UUID professor_id;

    private String cpf;

    private String professor_name;

    private String professor_academic_degree;

    @OneToOne(mappedBy = "coordinator")
    private  Course course;

    @OneToMany(mappedBy= "professor")
    private List<Subject> subjects; 
}
