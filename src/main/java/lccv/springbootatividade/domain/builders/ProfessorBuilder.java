package lccv.springbootatividade.domain.builders;

import lccv.springbootatividade.domain.dtos.ProfessorDTO;
import lccv.springbootatividade.domain.inputs.ProfessorInput;
import lccv.springbootatividade.domain.models.Professor;

import java.util.LinkedList;
import java.util.List;

public class ProfessorBuilder {

    public static Professor build(ProfessorInput input) {
        return Professor.builder()
                .professor_name(input.getProfessor_name())
                .cpf(input.getCpf())
                .professor_academic_degree(input.getProfessor_academic_degree())
                .build();
    }

    public static ProfessorDTO build(Professor student) {
        return ProfessorDTO.builder()
                .professor_id(student.getProfessor_id())
                .professor_name(student.getProfessor_name())
                .cpf(student.getCpf())
                .professor_academic_degree(student.getProfessor_academic_degree())
                .build();
    }

    public static List<ProfessorDTO> build(List<Professor> professors) {
        List<ProfessorDTO> finalList = new LinkedList<>();
        for (Professor professor : professors) {
            ProfessorDTO professorDTO = build(professor);
            finalList.add(professorDTO);
        }
        return finalList;
    }

}