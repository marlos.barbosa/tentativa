package lccv.springbootatividade.domain.builders;

import lccv.springbootatividade.domain.dtos.SubjectDTO;
import lccv.springbootatividade.domain.inputs.SubjectInput;
import lccv.springbootatividade.domain.models.Subject;

import java.util.LinkedList;
import java.util.List;

public class SubjectBuilder {

    public static Subject build(SubjectInput input) {
        return Subject.builder()
                .subject_name(input.getSubject_name())
                .code(input.getCode())
                .description(input.getDescription())
                .build();
    }

    public static SubjectDTO build(Subject student) {
        return SubjectDTO.builder()
                .subject_id(student.getSubject_id())
                .subject_name(student.getSubject_name())
                .code(student.getCode())
                .description(student.getDescription())
                .build();
    }

    public static List<SubjectDTO> build(List<Subject> subjects) {
        List<SubjectDTO> finalList = new LinkedList<>();
        for (Subject subject : subjects) {
            SubjectDTO SubjectDTO = build(subject);
            finalList.add(SubjectDTO);
        }
        return finalList;
    }

}