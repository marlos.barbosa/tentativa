package lccv.springbootatividade.domain.builders;

import lccv.springbootatividade.domain.dtos.CourseDTO;
import lccv.springbootatividade.domain.inputs.CourseInput;
import lccv.springbootatividade.domain.models.Course;

import java.util.LinkedList;
import java.util.List;

public class CourseBuilder {

    public static Course build(CourseInput input) {
        return Course.builder()
                .course_name(input.getCourse_name())
                .creation_date(input.getCreation_date())
                .building_name(input.getBuilding_name())
                .build();
    }

    public static CourseDTO build(Course student) {
        return CourseDTO.builder()
                .course_id(student.getCourse_id())
                .course_name(student.getCourse_name())
                .creation_date(student.getCreation_date())
                .building_name(student.getBuilding_name())
                .build();
    }

    public static List<CourseDTO> build(List<Course> courses) {
        List<CourseDTO> finalList = new LinkedList<>();
        for (Course course : courses) {
            CourseDTO CourseDTO = build(course);
            finalList.add(CourseDTO);
        }
        return finalList;
    }

}