package lccv.springbootatividade.domain.builders;

import lccv.springbootatividade.domain.dtos.StudentDTO;
import lccv.springbootatividade.domain.inputs.StudentInput;
import lccv.springbootatividade.domain.models.Student;

import java.util.LinkedList;
import java.util.List;

public class StudentBuilder {

    public static Student build(StudentInput input) {
        return Student.builder()
                .student_name(input.getStudent_name())
                .cpf(input.getCpf())
                .birthday_date(input.getBirthday_date())
                .rg(input.getRg())
                .dispatching_agency(input.getDispatching_agency())
                .build();
    }

    public static StudentDTO build(Student student) {
        return StudentDTO.builder()
            .student_id(student.getStudent_id())
            .student_name(student.getStudent_name())
            .cpf(student.getCpf())
            .birthday_date(student.getBirthday_date())
            .rg(student.getRg())
            .dispatching_agency(student.getDispatching_agency())
            .build();
    }

    public static List<StudentDTO> build(List<Student> students) {
        List<StudentDTO> finalList = new LinkedList<>();
        for (Student student : students) {
            StudentDTO studentDTO = build(student);
            finalList.add(studentDTO);
        }
        return finalList;
    }

}
