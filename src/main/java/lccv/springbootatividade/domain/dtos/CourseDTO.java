package lccv.springbootatividade.domain.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Representação de um curso.")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CourseDTO {

    @Schema(description = "ID do curso.")
    private UUID course_id;

    @Schema(description = "Nome do curso.")
    private String course_name;

    @Schema(description = "Data de criação do curso.")
    private String creation_date;

    @Schema(description = "Prédio do curso.")
    private String building_name;

}
