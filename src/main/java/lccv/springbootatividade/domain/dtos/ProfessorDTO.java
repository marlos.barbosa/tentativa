package lccv.springbootatividade.domain.dtos;

import java.util.UUID;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Schema(description = "Representação de um professor.")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProfessorDTO {
    private UUID professor_id;

    @Schema(description = "CPF do professor.")
    private String cpf;

    @Schema(description = "Nome do professor.")
    private String professor_name;

    @Schema(description = "Grau acadêmico do professor.")
    private String professor_academic_degree;
}
