package lccv.springbootatividade.domain.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;



@Schema(description = "Representação de um estudante.")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentDTO {

    private UUID student_id;

    @Schema(description = "CPF do estudante.")
    private String cpf;

    @Schema(description = "Nome do estudante.")
    private String student_name;

    @Schema(description = "Data de nascimento do estudante.")
    private String birthday_date;

    @Schema(description = "RG do estudante.")
    private String rg;

    @Schema(description = "Agência expedidora do RG do estudante.")
    private String dispatching_agency;  
}
