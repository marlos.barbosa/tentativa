package lccv.springbootatividade.domain.dtos;

import java.util.UUID;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Schema(description = "Representação de uma disciplina.")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SubjectDTO {
    private UUID subject_id;

    @Schema(description = "Nome da disciplina.")
    private String subject_name;

    @Schema(description = "Código da disciplina.")
    private String code;

    @Schema(description = "Descrição da disciplina.")
    private String description;
}
