package lccv.springbootatividade.domain.inputs;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "Representação de entrada de novo professor.")
public class ProfessorInput {
    @Schema(description = "Nome do professor.")
    @NotBlank(message = "Nome não pode ser vazio.")
    private String professor_name;

    @Schema(description = "CPF do professor.")
    @NotBlank(message = "CPF não pode ser vazio!")
    @Pattern(regexp = "^\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}$") //123.456.798-10
    private String cpf;

    @Schema(description = "Grau acadêmico do professor.")
    private String professor_academic_degree;
}
