package lccv.springbootatividade.domain.inputs;



import javax.validation.constraints.NotBlank;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "Representação de entrada de nova disciplina.")
public class SubjectInput {
    @Schema(description = "Nome da disciplia.")
    @NotBlank(message = "Nome não pode ser vazio.")
    private String subject_name;

    @Schema(description = "Código da disciplina.")
    private String code;

    @Schema(description = "Descrição da disciplina.")
    private String description;



}
