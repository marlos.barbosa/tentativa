package lccv.springbootatividade.domain.inputs;



import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "Representação de entrada de novo curso.")
public class CourseInput {

    @Schema(description = "Nome do curso.")
    @NotBlank(message = "Nome não pode ser vazio.")
    private String course_name;

    @Schema(description = "Data de criação do curso.")
    @Pattern(regexp = "^\\d{2}\\/\\d{2}\\/\\d{4}$") //26/01/2000
    private String creation_date;

    @Schema(description = "Prédio do curso.")
    private String building_name;


}
