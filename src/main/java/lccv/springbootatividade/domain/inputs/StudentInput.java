package lccv.springbootatividade.domain.inputs;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;


@Data
@Schema(description = "Representação de entrada de novo estudante.")
public class StudentInput {

    @Schema(description = "Nome do estudante.")
    @NotBlank(message = "Nome não pode ser vazio!")
    private String student_name;

    @Schema(description = "CPF do estudante.")
    @NotBlank(message = "CPF não pode ser vazio!")
    @Pattern(regexp = "^\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}$") //123.456.798-10
    private String cpf;

    @Schema(description = "Data de nascimento do estudante.")
    @Pattern(regexp = "^\\d{2}\\/\\d{2}\\/\\d{4}$") //26/01/2000
    private String birthday_date;

    @Schema(description = "RG do estudante.")
    private String rg;

    @Schema(description = "Agência expedidora do RG do estudante.")
    private String dispatching_agency;


}
