package lccv.springbootatividade;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAtividadeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAtividadeApplication.class, args);
	}

}
