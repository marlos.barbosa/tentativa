package lccv.springbootatividade.services;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lccv.springbootatividade.domain.builders.CourseBuilder;
import lccv.springbootatividade.domain.inputs.CourseInput;
import lccv.springbootatividade.domain.models.Course;
import lccv.springbootatividade.domain.models.Professor;
import lccv.springbootatividade.domain.models.Student;
import lccv.springbootatividade.repositories.CourseRepository;
import lccv.springbootatividade.repositories.ProfessorRepository;
import lombok.RequiredArgsConstructor;


@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CourseService {
    private final CourseRepository courseRepository;
    private final ProfessorRepository professorRepository;

    public Course create(CourseInput courseInput){
        Course course = CourseBuilder.build(courseInput);
        course = courseRepository.save(course);
        return course;
    }

    public List<Course> findAll(){
        List<Course> allCourses = courseRepository.findAll();
        return allCourses;
    }

    public Course findOne(UUID course_id) {
        Course course = courseRepository.getReferenceById(course_id);
        return course;
    }

    public Course changeCoordinator(UUID professor_id, UUID course_id){
        Professor professor = professorRepository.getReferenceById(professor_id);
        Course course = courseRepository.getReferenceById(course_id);
        course.setCoordinator(professor);
        courseRepository.save(course);
        return course;
    }

    public List<Student> studentsFromCourse(UUID course_id){
        Course course = courseRepository.getReferenceById(course_id);
        List<Student> studentsFromCourse = course.getStudents();
        return studentsFromCourse;
    }
}