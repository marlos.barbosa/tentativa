package lccv.springbootatividade.services;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lccv.springbootatividade.domain.builders.SubjectBuilder;
import lccv.springbootatividade.domain.inputs.SubjectInput;
import lccv.springbootatividade.domain.models.Professor;
import lccv.springbootatividade.domain.models.Student;
import lccv.springbootatividade.domain.models.Subject;
import lccv.springbootatividade.repositories.ProfessorRepository;
import lccv.springbootatividade.repositories.SubjectRepository;
import lombok.RequiredArgsConstructor;


@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SubjectService {
    private final SubjectRepository subjectRepository;
    private final ProfessorRepository professorRepository;

    public Subject create(SubjectInput subjectInput){
        Subject subject = SubjectBuilder.build(subjectInput);
        subject = subjectRepository.save(subject);
        return subject;
    }

    public List<Subject> findAll(){
        List<Subject> allSubjects = subjectRepository.findAll();
        return allSubjects;
    }

    public Subject findOne(UUID subject_id) {
        Subject subject = subjectRepository.getReferenceById(subject_id);
        return subject;
    }

    public Subject changeProfessor(UUID professor_id, UUID subject_id){
        Professor professor = professorRepository.getReferenceById(professor_id);
        Subject subject = subjectRepository.getReferenceById(subject_id);
        subject.setProfessor(professor);
        subjectRepository.save(subject);
        return subject;
    }

    public List<Student> studentsFromSubject(UUID subject_id){
        Subject subject = subjectRepository.getReferenceById(subject_id);
        List<Student> studentsFromSubject = subject.getStudents();
        return studentsFromSubject;
    }
}