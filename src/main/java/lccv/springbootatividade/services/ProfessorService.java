package lccv.springbootatividade.services;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lccv.springbootatividade.domain.builders.ProfessorBuilder;
import lccv.springbootatividade.domain.inputs.ProfessorInput;
import lccv.springbootatividade.domain.models.Professor;
import lccv.springbootatividade.repositories.ProfessorRepository;
import lombok.RequiredArgsConstructor;


@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProfessorService {
    private final ProfessorRepository professorRepository;

    public Professor create(ProfessorInput professorInput){
        Professor professor = ProfessorBuilder.build(professorInput);
        professor = professorRepository.save(professor);
        return professor;
    }

    public List<Professor> findAll(){
        List<Professor> allProfessors = professorRepository.findAll();
        return allProfessors;
    }

    public Professor findOne(UUID professor_id) {
        Professor professor = professorRepository.getReferenceById(professor_id);
        return professor;
    }
}