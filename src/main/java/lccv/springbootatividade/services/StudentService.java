package lccv.springbootatividade.services;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lccv.springbootatividade.domain.builders.StudentBuilder;
import lccv.springbootatividade.domain.inputs.StudentInput;
import lccv.springbootatividade.domain.models.Course;
import lccv.springbootatividade.domain.models.Student;
import lccv.springbootatividade.domain.models.Subject;
import lccv.springbootatividade.repositories.CourseRepository;
import lccv.springbootatividade.repositories.StudentRepository;
import lccv.springbootatividade.repositories.SubjectRepository;
import lombok.RequiredArgsConstructor;


@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class StudentService {
    private final StudentRepository studentRepository;
    private final CourseRepository courseRepository;
    private final SubjectRepository subjectRepository;

    public Student create(StudentInput studentInput){
        Student student = StudentBuilder.build(studentInput);
        student = studentRepository.save(student);
        return student;
    }

    public List<Student> findAll(){
        List<Student> allStudents = studentRepository.findAll();
        return allStudents;
    }

    public Student findOne(UUID student_id) {
        Student student = studentRepository.getReferenceById(student_id);
        return student;
    }

    public Student changeCourse(UUID student_id, UUID course_id){
        Student student = studentRepository.getReferenceById(student_id);
        Course course = courseRepository.getReferenceById(course_id);
        student.setCourse(course);
        studentRepository.save(student);
        return student;
    }

    public Student enrollStudentClass(UUID student_id, UUID subject_id){
        Student student = studentRepository.getReferenceById(student_id);
        List<Subject> subjects = student.getSubjects();
        Subject subject = subjectRepository.getReferenceById(subject_id);
        List<Student> students = subject.getStudents();
        students.add(student);
        subjects.add(subject);
        student.setSubjects(subjects);
        subject.setStudents(students);
        studentRepository.save(student);
        subjectRepository.save(subject);
        return student;
    }

    public Student removeStudentClass(UUID student_id, UUID subject_id){
        Student student = studentRepository.getReferenceById(student_id);
        List<Subject> subjects = student.getSubjects();
        Subject subject = subjectRepository.getReferenceById(subject_id);
        List<Student> students = subject.getStudents();
        students.remove(student);
        subjects.remove(subject);
        student.setSubjects(subjects);
        subject.setStudents(students);
        studentRepository.save(student);
        subjectRepository.save(subject);
        return student;
    }
}