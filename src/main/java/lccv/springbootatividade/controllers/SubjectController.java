package lccv.springbootatividade.controllers;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lccv.springbootatividade.domain.builders.StudentBuilder;
import lccv.springbootatividade.domain.builders.SubjectBuilder;
import lccv.springbootatividade.domain.dtos.StudentDTO;
import lccv.springbootatividade.domain.dtos.SubjectDTO;
import lccv.springbootatividade.domain.inputs.SubjectInput;
import lccv.springbootatividade.domain.models.Student;
import lccv.springbootatividade.domain.models.Subject;
import lccv.springbootatividade.services.SubjectService;
import lombok.RequiredArgsConstructor;

@Tag(name = "Subjects", description = "Endpoints da entidade Subject")
@RestController
@RequestMapping("/subjects")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SubjectController {
    private final SubjectService service;

    @Operation(
        summary = "Criar nova disciplina",
        description = "Adiciona uma disciplina ao banco de dados."
    )
    @PostMapping("/")
    public SubjectDTO create(@Valid @RequestBody SubjectInput subjectInput){
        Subject subject = service.create(subjectInput);
        return SubjectBuilder.build(subject);
    }

    @Operation(
        summary = "Coletar dados de todos as disciplina",
        description = "Coleta lista com os dados de cada disciplina"
    )
    @GetMapping
    public List<SubjectDTO> findAll(){
        List<Subject> allSubjects = service.findAll();
        return SubjectBuilder.build(allSubjects);
    }

    @Operation(
        summary = "Coletar dados de uma disciplina",
        description = "Coleta os dados de apenas uma disciplina"
    )
    @GetMapping("/{subject_id}")
    public SubjectDTO findOne(@PathVariable UUID subject_id){
        Subject subject = service.findOne(subject_id);
        return SubjectBuilder.build(subject);
    }

    @Operation(
        summary = "Atribuir professor a uma disciplina",
        description = "Associa cordenador a uma disciplina específica"
    )
    @PutMapping("/{professor_id}/{subject_id}")
    public SubjectDTO changeCoordinator(@PathVariable UUID professor_id, @PathVariable UUID subject_id){
        Subject subject = service.changeProfessor(professor_id, subject_id);
        return SubjectBuilder.build(subject);
    }

    @Operation(
        summary = "Coletar lista com os dados de todos os estudantes de uma disciplina",
        description = "Coleta dados de todos os estudantes matriculados em uma disciplina"
    )
    @GetMapping("/students/{subject_id}")
    public List<StudentDTO> stundetFromCourse(@PathVariable UUID subject_id){
        List<Student> students = service.studentsFromSubject(subject_id);
        return StudentBuilder.build(students);
    }
}