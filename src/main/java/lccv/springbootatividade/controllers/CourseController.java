package lccv.springbootatividade.controllers;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lccv.springbootatividade.domain.builders.CourseBuilder;
import lccv.springbootatividade.domain.builders.StudentBuilder;
import lccv.springbootatividade.domain.dtos.CourseDTO;
import lccv.springbootatividade.domain.dtos.StudentDTO;
import lccv.springbootatividade.domain.inputs.CourseInput;
import lccv.springbootatividade.domain.models.Course;
import lccv.springbootatividade.domain.models.Student;
import lccv.springbootatividade.services.CourseService;
import lombok.RequiredArgsConstructor;

@Tag(name = "Courses", description = "Endpoints da entidade Course")
@RestController
@RequestMapping("/courses")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CourseController {
    private final CourseService service;

    @Operation(
        summary = "Criar novo curso",
        description = "Adiciona um curso ao banco de dados."
    )
    @PostMapping("/")
    public CourseDTO create(@Valid @RequestBody CourseInput courseInput){
        Course course = service.create(courseInput);
        return CourseBuilder.build(course);
    }

    @Operation(
        summary = "Coletar dados de todos os cursos",
        description = "Coleta lista com os dados de cada curso"
    )
    @GetMapping
    public List<CourseDTO> findAll(){
        List<Course> allCourses = service.findAll();
        return CourseBuilder.build(allCourses);
    }

    @Operation(
        summary = "Coletar dados de um curso",
        description = "Coleta os dados de apenas um curso"
    )
    @GetMapping("/{course_id}")
    public CourseDTO findOne(@PathVariable UUID course_id){
        Course course = service.findOne(course_id);
        return CourseBuilder.build(course);
    }

    @Operation(
        summary = "Atribuir cordenador a um curso",
        description = "Associa cordenador a um curso específico"
    )
    @PutMapping("/{professor_id}/{course_id}")
    public CourseDTO changeCoordinator(@PathVariable UUID professor_id, @PathVariable UUID course_id){
        Course course = service.changeCoordinator(professor_id, course_id);
        return CourseBuilder.build(course);
    }

    @Operation(
        summary = "Coletar lista com os dados de todos os estudantes de um curso",
        description = "Coleta dados de todos os estudantes matriculados em um curso"
    )
    @GetMapping("/students/{course_id}")
    public List<StudentDTO> stundetFromCourse(@PathVariable UUID course_id){
        List<Student> students = service.studentsFromCourse(course_id);
        return StudentBuilder.build(students);
    }
}