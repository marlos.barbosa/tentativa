package lccv.springbootatividade.controllers;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lccv.springbootatividade.domain.builders.ProfessorBuilder;
import lccv.springbootatividade.domain.dtos.ProfessorDTO;
import lccv.springbootatividade.domain.inputs.ProfessorInput;
import lccv.springbootatividade.domain.models.Professor;
import lccv.springbootatividade.services.ProfessorService;
import lombok.RequiredArgsConstructor;

@Tag(name = "Professors", description = "Endpoints da entidade Professor")
@RestController
@RequestMapping("/professors")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProfessorController {
    private final ProfessorService service;

    @Operation(
        summary = "Criar novo professor",
        description = "Adiciona novo professor ao banco de dados"
    )
    @PostMapping("/")
    public ProfessorDTO create(@Valid @RequestBody ProfessorInput professorInput){
        Professor professor = service.create(professorInput);
        return ProfessorBuilder.build(professor);
    }

    @Operation(
        summary = "Coletar dados de todos os professores",
        description = "Coleta lista com os dados de cada professor"
    )
    @GetMapping
    public List<ProfessorDTO> findAll(){
        List<Professor> allProfessors = service.findAll();
        return ProfessorBuilder.build(allProfessors);
    }

    @Operation(
        summary = "Coletar dados de um professor",
        description = "Coleta os dados de apenas um professor"
    )
    @GetMapping("/{professor_id}")
    public ProfessorDTO findOne(@PathVariable UUID professor_id){
        Professor professor = service.findOne(professor_id);
        return ProfessorBuilder.build(professor);
    }
}
