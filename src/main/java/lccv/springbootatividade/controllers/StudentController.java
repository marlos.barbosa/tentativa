package lccv.springbootatividade.controllers;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lccv.springbootatividade.domain.builders.StudentBuilder;
import lccv.springbootatividade.domain.dtos.StudentDTO;
import lccv.springbootatividade.domain.inputs.StudentInput;
import lccv.springbootatividade.domain.models.Student;
import lccv.springbootatividade.services.StudentService;
import lombok.RequiredArgsConstructor;

@Tag(name = "Students", description = "Endpoints da entidade Student")
@RestController
@RequestMapping("/students")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class StudentController {
    private final StudentService service;

    @Operation(
        summary = "Criar novo estudante",
        description = "Adiciona novo estudante ao banco de dados"
    )
    @PostMapping("/")
    public StudentDTO create(@Valid @RequestBody StudentInput studentInput){
        Student student = service.create(studentInput);
        return StudentBuilder.build(student);
    }

    @Operation(
        summary = "Coletar dados de todos os estudantes",
        description = "Coleta lista com os dados de cada estudante"
    )
    @GetMapping
    public List<StudentDTO> findAll(){
        List<Student> allStudents = service.findAll();
        return StudentBuilder.build(allStudents);
    }

    @Operation(
        summary = "Coletar dados de um estudante",
        description = "Coleta os dados de apenas um estudante"
    )
    @GetMapping("/{student_id}")
    public StudentDTO findOne(@PathVariable UUID student_id){
        Student student = service.findOne(student_id);
        return StudentBuilder.build(student);
    }

    @Operation(
        summary = "Atribuir curso a um estudando",
        description = "Associa estudante a um curso específico"
    )
    @PutMapping("/{student_id}/{course_id}")
    public StudentDTO changeCoordinator(@PathVariable UUID student_id, @PathVariable UUID course_id){
        Student student = service.changeCourse(student_id, course_id);
        return StudentBuilder.build(student);
    }

    @Operation(
        summary = "Matricular estudante em uma disciplina",
        description = "Associar estudante a uma disciplina específica"
    )
    @PostMapping("/{student_id}/{subject_id}")
    public StudentDTO enrollStudentClass (@PathVariable UUID student_id,@PathVariable UUID subject_id){
        Student student = service.enrollStudentClass(student_id, subject_id);
        return StudentBuilder.build(student);
    }

    @Operation(
        summary = "Desmatricular estudante de uma disciplina",
        description = "Desassociar estudante de uma disciplina específica"
    )
    @DeleteMapping("/{student_id}/{subject_id}/")
    public StudentDTO removeStudentClass (@PathVariable UUID student_id,@PathVariable UUID subject_id){
        Student student = service.removeStudentClass(student_id, subject_id);
        return StudentBuilder.build(student);
    }
}